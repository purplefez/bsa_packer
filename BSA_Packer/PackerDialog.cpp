#include "PackerDialog.h"

// Qt Widgets
#include <QMessageBox>
#include <QPushButton>

constexpr uint32_t DIALOG_WIDTH = 240;
constexpr uint32_t DIALOG_HEIGHT = 200;
constexpr uint32_t COMBO_MIN_WIDTH = 100;
constexpr uint32_t LIST_MIN_HEIGHT = 100;

PackerDialog::PackerDialog(QWidget* parent)
	: QDialog(parent),
	buttons_ok_cancel_mod(QDialogButtonBox::Ok | QDialogButtonBox::Cancel)
{
	QPushButton* ok = buttons_ok_cancel_mod.button(QDialogButtonBox::Ok);
	ok->setEnabled(false);

	label_choose_mod.setText(tr("Choose a mod to pack:"));
	label_choose_name.setText(tr("Choose the name of the packed archive:"));

	this->resize(DIALOG_WIDTH, DIALOG_HEIGHT);
	this->setWindowTitle(tr("BSA Packer"));

	combo_mod_list.setMinimumWidth(COMBO_MIN_WIDTH);

	list_archive_names.setMinimumHeight(LIST_MIN_HEIGHT);

	layout_hor.addStretch(1);
	layout_hor.addWidget(&buttons_ok_cancel_mod);

	layout_vert.addWidget(&label_choose_mod);
	layout_vert.addWidget(&combo_mod_list);
	layout_vert.addWidget(&label_choose_name);
	layout_vert.addWidget(&list_archive_names);
	layout_vert.addLayout(&layout_hor);

	this->setLayout(&layout_vert);

	connect(&list_archive_names, &QListWidget::itemSelectionChanged, this, &PackerDialog::update_ok_button, Qt::QueuedConnection);
	connect(&list_archive_names, qOverload<QListWidgetItem*>(&QListWidget::itemDoubleClicked), this, &PackerDialog::accept);
	connect(&buttons_ok_cancel_mod, &QDialogButtonBox::accepted, this, &PackerDialog::accept);
	connect(&buttons_ok_cancel_mod, &QDialogButtonBox::rejected, this, &PackerDialog::reject);
}

void PackerDialog::update_mod_list(const QStringList& mods)
{
	combo_mod_list.clear();
	combo_mod_list.addItems(mods);
}

void PackerDialog::update_name_list(const QStringList& names)
{
	list_archive_names.clear();
	list_archive_names.addItems(names);
}

QString PackerDialog::read_selected_mod() const
{
	return combo_mod_list.currentText();
}

const QListWidgetItem* PackerDialog::read_selected_name() const
{
	return list_archive_names.currentItem();
}

void PackerDialog::update_selected_name()
{
	combo_mod_list.currentIndexChanged(combo_mod_list.currentText());
}

void PackerDialog::update_ok_button()
{
	QPushButton* ok = buttons_ok_cancel_mod.button(QDialogButtonBox::Ok);
	ok->setEnabled(list_archive_names.currentItem() != nullptr);
}