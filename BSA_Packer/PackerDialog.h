#ifndef PACKER_DIALOG_H
#define PACKER_DIALOG_H

// Qt Widgets
#include <QBoxLayout>
#include <QComboBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>
#include <QListWidget>

/**
 * @brief packer dialog desc
 */
class PackerDialog : public QDialog
{
	Q_OBJECT

public:
	PackerDialog(QWidget* parent = nullptr);

	/**
	 * @brief
	 * @return const pointer to combo_mod_list.
	 */
	const QComboBox* get_combo_mod_list() const { return &combo_mod_list; };

	/**
	 * @brief Replaces the combobox with the contents of mods.
	 * @see BSA_Packer::display()
	 */
	void update_mod_list(const QStringList& mods);

	/**
	 * @brief Replaces the listview with the contents of names.
	 * @see BSA_Packer::combo_item_changed_handler()
	 */
	void update_name_list(const QStringList& names);

	/**
	 * @brief Reads the currently selected item from the combobox.
	 * @return The name of the mod.
	 */
	QString read_selected_mod() const;

	/**
	 * @brief Reads the currently selected item from the listview.
	 * @return A potential name for the archive.
	 */
	const QListWidgetItem* read_selected_name() const;

	/**
	 * @brief Invokes currentIndexChanged event which in turn calls the
	 * combo_item_changed_handler and updates the ok button.
	 * @see BSA_Packer::combo_item_changed_handler()
	 */
	void update_selected_name();

public slots:
	/**
	 * @brief Enables or disables the button based on whether the list of names
	 * has an option selected.
	 * @see BSA_Packer::list_selection_changed_handler()
	 */
	void update_ok_button();

private:
	QComboBox combo_mod_list;
	QLabel label_choose_mod;
	QLabel label_choose_name;
	QListWidget list_archive_names;
	QVBoxLayout layout_vert;
	QHBoxLayout layout_hor;
	QDialogButtonBox buttons_ok_cancel_mod;
};

#endif // PACKER_DIALOG_H
