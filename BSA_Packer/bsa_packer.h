#ifndef BSA_PACKER_H
#define BSA_PACKER_H

// Mod Organizer
#include <iplugintool.h>

// User interface
#include <PackerDialog.h>

// libbsarch
#include <BSArchiveAuto.h>
#include <QProgressDialog>

/**
 * @brief A tool to transform loose files into a Bethesda Softworks Archive
 * file (.bsa/.ba2). As well as providing helpful functions such as hiding the
 * loose assets after packing, and creating plugins to load the archive.
 */
class BSA_Packer : public MOBase::IPluginTool
{
	Q_OBJECT
		Q_INTERFACES(MOBase::IPlugin MOBase::IPluginTool)
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
		Q_PLUGIN_METADATA(IID "org.MattyFez.BSA_Packer" FILE "bsa_packer.json")
#endif

public:

	/**
	 * @brief An enum value to represent the nexus mods identifier.
	 */
	enum class NEXUS_ID
	{
		UNDEFINED = 0,		/**< Provides a default value for a NEXUS_ID. */
		MORROWIND = 100,	/**< The managed game is Morrowind. */
		OBLIVION = 101,		/**< The managed game is Oblivion. */
		FALLOUT_3 = 120,	/**< The managed game is Fallout 3. */
		NEW_VEGAS = 130,	/**< The managed game is Fallout: New Vegas. */
		SKYRIM = 110,		/**< The managed game is Skyrim. */
		SKYRIM_SE = 1704,	/**< The managed game is Skyrim Special Edition. */
		FALLOUT_4 = 1151	/**< The managed game is Fallout 4. */
	};

	/**
	 * @brief A series of return codes for error handling.
	 */
	enum class PACKER_RESULT
	{
		SUCCESS = 0,			/**< Denotes successful completion of the task. */
		NO_ITEM_SELECTED,		/**< User did not choose item from the listview. */
		INVALID_NAME,			/**< User chose an invalid name for the archive. */
		CANCELLED_OVERWRITE,	/**< User cancelled the overwrite operation. */
		ARCHIVE_OVER_LIMIT,		/**< Archive exceeds size limit. */
		NULL_BSA,				/**< Archive is uninitialised. */
		ABORTED_PACKING
	};

	BSA_Packer();

	// Inherited via IPlugin
	bool init(MOBase::IOrganizer* moInfo) override;
	QString name() const override;
	QString author() const override;
	QString description() const override;
	MOBase::VersionInfo version() const override;
	bool isActive() const override;
	QList<MOBase::PluginSetting> settings() const override;

	// Inherited via IPluginTool
	QString tooltip() const override;
	QIcon icon() const override;
	QString displayName() const override;

private slots:

	void display() const override;

	/**
	 * @brief Handles the selection of a new mod in the combobox.
	 * @param mod_name The name of the mod to have its loose files archived.
	 */
	void combo_item_changed_handler(const QString&);

private:

	/**
	 * @brief Callback method to populate dds_info using DirectXTex functions.
	 * @param archive Unused parameter, but required by the
	 * bsa_file_dds_info_proc_t typedef.
	 * @param file_path Relative path to the texture (this is a bug, will soon
	 * be fixed to be the absolute path).
	 * @param dds_info A struct to hold the width, height, and mipLevels of the
	 * DDS file.
	 */
	static void BSA_Packer::DDSCallback(bsa_archive_t, const wchar_t*, bsa_dds_info_t*, void*);

	static QString get_filename_from_fileinfo(const QFileInfo&);

	static void hide_folder(const QString&, const QString&);

	static bool is_mod_state_valid(const MOBase::IModList*, const QString&);

	static const unsigned char* raw_data(NEXUS_ID);

	static uint8_t raw_size(NEXUS_ID);

	/**
	 * @brief Populates archive with all the files within mod_directory.
	 * @param mod_path The path to the mod folder.
	 * @param archive Holds file data so they can be packed into an archive.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT build_archive_all(QProgressDialog&, const QString&, BSArchiveAuto&) const;

	/**
	 * @brief Populates archive with all DDS files within mod_directory.
	 * @param mod_path The path to the mod folder.
	 * @param archive Holds file data so they can be packed into an archive.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT build_archive_textures(QProgressDialog&, const QString*, BSArchiveAuto&) const;

	/**
	 * @brief Populates archive with all non-DDS files within mod_directory.
	 * @param mod_path The path to the mod folder.
	 * @param archive Holds file data so they can be packed into an archive.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT build_archive_no_textures(QProgressDialog&, const QString&, BSArchiveAuto&) const;

	/**
	 * @brief Determines whether the process should overwrite an existing file.
	 * @param mod_path The path to the mod folder.
	 * @param archive_name The name of the archive with a file extension.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT can_overwrite_archives(const QString&, const QString&) const;

	/**
	 * @brief Determines whether the archive produced exceeds the file size
	 * limit.
	 * @param archive_fullname The absolute path and name of the archive.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT check_archive_size(const QString&) const;

	/**
	 * @brief Calls all the functions required for the packing process.
	 * @param mod_path The path to the mod folder.
	 * @param archive_base_name The name of the archive with no file extension.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT create_archive(const QString&, const QString&) const;

	/**
	 * @brief Produces a dummy ESP to load the archive if necessary.
	 * @param mod_path The path to the mod folder.
	 * @param archive_name_base The name of the archive with no file extension.
	 */
	void create_dummy_plugin(const QString&, const QString&) const;

	/**
	 * @brief Determines the type of archive being made.
	 * @return An enum to be passed to the archive builder function.
	 */
	bsa_archive_type_e find_archive_type(NEXUS_ID) const;

	/**
	 * @brief Determines the maximum size of the archive for the current game.
	 * @param nexus_id The nexus identifier for the game.
	 * @return The size limit in bytes.
	 */
	uint32_t get_archive_size_limit(NEXUS_ID) const;

	/**
	 * @brief Lists every entry of a file in a directory.
	 * @param mod_directory The path to the mod folder.
	 * @return A list of file names in the specified directory.
	 */
	QStringList get_directory_files(const QDir&) const;

	/**
	 * @brief Uses the names of plugins to make a list of archive names.
	 * @param mod_directory The path to the mod folder.
	 * @return A list of potential archive names.
	 */
	QStringList get_mod_plugin_names(const QDir&) const;

	/**
	 * @brief Lists every mod filtered by STATE_VALID.
	 * @return A list of mods that may have their contents packed.
	 */
	QStringList get_valid_mods() const;

	/**
	 * @brief Appends ".mohidden" to the folders within a mod directory.
	 * @param mod_directory The path to the mod folder.
	 */
	void hide_loose_files(const QDir&) const;

	/**
	 * @brief Checks whether the filename ends with a blacklisted extension.
	 * @param filename The name of the file to be checked.
	 * @return Whether the file should be ignored.
	 */
	bool is_blacklisted_filetype(const QString&) const;

	/**
	 * @brief Checks the file extension of a filename and determines whether it
	 * is a compressible type.
	 * @param filename The name of the file to be checked.
	 * @return Whether the file should cause the archvie to be uncompressed.
	 */
	bool is_incompressible(const QString&) const;

	/**
	 * @brief Sets variables dependent upon the managed game.
	 * @param nexus_id The nexus identifier for the game.
	 */
	void set_nexus_id(NEXUS_ID);

	/**
	 * @brief Validates the name chosen by the user for the archive.
	 * @param mod_name The name of the mod.
	 * @param archive_name_base The name of the archive with no file extension.
	 * @return The result of performing this operation.
	 */
	PACKER_RESULT valid_archive_name(const QString&, QString*) const;

private:

	/**
	 * The nexusmods identifier of the managed game.
	 */
	NEXUS_ID m_nexus_id = NEXUS_ID::UNDEFINED;

	/**
	 * Handles all mod organizer functions.
	 */
	const MOBase::IOrganizer* m_Organizer = nullptr;

	/**
	 * Handles the dialog boxes and user interface.
	 */
	const std::unique_ptr<PackerDialog> packer_dialog;

	/**
	 * The file extension used by the archives of the managed game.
	 */
	const QString* m_extension = nullptr;

	/**
	 * The "new filename" string with the appropriate extension.
	 */
	const QString* m_new_archive = nullptr;

	/**
	 * Whether to create a dummy plugin to load the archive (if one does not
	 * already exist).
	 */
	bool should_create_plugin = false;

	/**
	 * Whether to hide assets which have just been packed by this plugin.
	 */
	bool should_hide_loose = false;
};

#endif // BSA_PACKER_H
