#include "bsa_packer.h"

#include "BSA_CONSTS.h"

#include "DirectXTex.h"

#include <array>
#include <cstdio>

// Mod Organizer
#include "imodlist.h"
#include "imodinterface.h"
#include "iplugingame.h"

// Qt Core
#include <QDirIterator>
#include <QFlags>

// Qt Widgets
#include <QInputDialog>
#include <QMessageBox>
#include <QProgressDialog>

// Qt Concurrent
#include <QtConcurrentFilter>
#include <QtConcurrentMap>

using namespace bsa_consts;
using namespace MOBase;

const QString& SETTING_ENABLED = "enabled";
const QString& SETTING_HIDE_LOOSE_ASSETS = "hide_loose_assets";
const QString& SETTING_CREATE_PLUGINS = "create_plugins";
const QString& SETTING_BLACKLISTED_FILES = "blacklisted_files";
const QString& SETTING_SPLIT_ARCHIVES = "split_archives";
const QString& EXTENSION_BSA = ".bsa";
const QString& EXTENSION_BA2 = ".ba2";
const QString& NEW_BSA = "<new filename>" + EXTENSION_BSA;
const QString& NEW_BA2 = "<new filename>" + EXTENSION_BA2;
const QString& TEXTURES_BA2 = " - Textures" + EXTENSION_BA2;
const std::array <QString, 3> INCOMPRESSIBLE_TYPES = { "wav", "ogg", "mp3" };
const QStringList& PLUGIN_TYPES = { "*.esm", "*.esp", "*.esl" };

constexpr uint32_t BSA_MAX_SIZE = INT32_MAX;
constexpr uint32_t BA2_MAX_SIZE = UINT32_MAX;

BSA_Packer::BSA_Packer()
	: packer_dialog(std::make_unique<PackerDialog>())
{
	connect(packer_dialog->get_combo_mod_list(), qOverload<const QString&>(&QComboBox::currentIndexChanged), this, [this](auto&& text) { combo_item_changed_handler(text); });
}

bool BSA_Packer::init(IOrganizer* moInfo)
{
	m_Organizer = moInfo;
	return true;
}

QString BSA_Packer::name() const
{
	return "BSA Packer";
}

QString BSA_Packer::author() const
{
	return "MattyFez";
}

QString BSA_Packer::description() const
{
	return tr("Transform loose files into a Bethesda Softworks Archive file (.bsa/.ba2).");
}

VersionInfo BSA_Packer::version() const
{
	return VersionInfo(1, 0, 0, VersionInfo::RELEASE_FINAL);
}

bool BSA_Packer::isActive() const
{
	return m_Organizer->pluginSetting(this->name(), SETTING_ENABLED).toBool();
}

QList<PluginSetting> BSA_Packer::settings() const
{
	// settings sorted alphabetically
	return QList<PluginSetting>()
		<< PluginSetting(SETTING_HIDE_LOOSE_ASSETS, tr("After creating the archive, set loose assets to hidden."), false)
		<< PluginSetting(SETTING_CREATE_PLUGINS, tr("Create a dummy plugin to load the archive if one does not exist."), false)
		<< PluginSetting(SETTING_ENABLED, tr("Allow this plugin to run."), false)
		<< PluginSetting(SETTING_BLACKLISTED_FILES, tr("Specify a semi-colon seperated list of file extensions to ignore when packing."), ".txt;.hkx;.xml;.ini")
		//<< PluginSetting(SETTING_SPLIT_ARCHIVES, tr("Automatically create multiple archives if they exceed the size limit."), false);
		;
}

QString BSA_Packer::tooltip() const
{
	return tr("Transform loose files into a Bethesda Softworks Archive file (.bsa/.ba2).");
}

QIcon BSA_Packer::icon() const
{
	return QIcon();
}

QString BSA_Packer::displayName() const
{
	return tr("BSA Packer");
}

void BSA_Packer::display() const
{
	// get filenames of valid mods
	const QStringList& mods_valid = get_valid_mods();
	packer_dialog->update_mod_list(mods_valid);

	// wait for user to complete dialog prompt
	if (packer_dialog->exec() == QDialog::DialogCode::Rejected)
		return;

	QString archive_name_base;
	PACKER_RESULT result;

	// get the name of the mod being packed
	const QString& mod_name = packer_dialog->read_selected_mod();
	const QString& mod_dir = m_Organizer->getMod(mod_name)->absolutePath();

	// ensure the name of the archive is valid
	result = valid_archive_name(mod_name, &archive_name_base);
	if (result != PACKER_RESULT::SUCCESS)
		return;

	// create archive
	result = create_archive(mod_dir, archive_name_base);
	if (result != PACKER_RESULT::SUCCESS)
		return;

	// create dummy plugin
	if (m_nexus_id != NEXUS_ID::UNDEFINED &&
		m_nexus_id != NEXUS_ID::MORROWIND &&
		should_create_plugin)
		create_dummy_plugin(mod_dir, archive_name_base);

	// hide loose files
	if (should_hide_loose)
		hide_loose_files(mod_dir);

	// deselect name before returning, also disables the ok button again
	packer_dialog->update_selected_name();
}

void BSA_Packer::combo_item_changed_handler(const QString& mod_name)
{
	if (mod_name == nullptr)
		return;

	should_hide_loose = m_Organizer->pluginSetting(this->name(), SETTING_HIDE_LOOSE_ASSETS).toBool();
	should_create_plugin = m_Organizer->pluginSetting(this->name(), SETTING_CREATE_PLUGINS).toBool();

	// nexus id is set in this method as it is the only non const and it cannot be done in the init()
	if (m_nexus_id == NEXUS_ID::UNDEFINED)
		set_nexus_id(static_cast<NEXUS_ID>(m_Organizer->managedGame()->nexusGameID()));

	const QDir& mod_directory(m_Organizer->getMod(mod_name)->absolutePath());
	const QStringList& filenames = this->get_mod_plugin_names(mod_directory);
	packer_dialog->update_name_list(filenames);
}

void BSA_Packer::DDSCallback(bsa_archive_t archive,
							 const wchar_t* file_path,
							 bsa_dds_info_t* dds_info,
							 void* context)
{
	auto image = std::make_unique<DirectX::ScratchImage>();
	DirectX::TexMetadata info;

	const QString path = *static_cast<QString*>(context) + '/' + QString::fromStdWString(file_path);
	const auto hr = LoadFromDDSFile(PREPARE_PATH_LIBBSARCH(path), DirectX::DDS_FLAGS_NONE, &info, *image);

	if (FAILED(hr))
		throw std::runtime_error("Failed to open DDS");

	dds_info->width = static_cast<uint32_t>(info.width);
	dds_info->height = static_cast<uint32_t>(info.height);
	dds_info->mipmaps = static_cast<uint32_t>(info.mipLevels);
}

QString BSA_Packer::get_filename_from_fileinfo(const QFileInfo& fi)
{
	return fi.fileName();
}

void BSA_Packer::hide_folder(const QString& abs_mod_path,
							 const QString& sub_dir)
{
	const QString& absolute_path = abs_mod_path + '/' + sub_dir;
	QDir dir(absolute_path);
	if (!dir.dirName().endsWith(".mohidden"))
		dir.rename(absolute_path, absolute_path + ".mohidden");
}

bool BSA_Packer::is_mod_state_valid(const IModList* const mod_list,
									const QString& mod)
{
	return mod_list->state(mod) & IModList::STATE_VALID;
}

const unsigned char* BSA_Packer::raw_data(const NEXUS_ID nexus_id)
{
	switch (nexus_id) {
		case NEXUS_ID::OBLIVION:
			return RAW_OBLIVION;
		case NEXUS_ID::FALLOUT_3:
			return RAW_FALLOUT3;
		case NEXUS_ID::NEW_VEGAS:
			return RAW_NEWVEGAS;
		case NEXUS_ID::SKYRIM:
			return RAW_SKYRIM;
		case NEXUS_ID::SKYRIM_SE:
			return RAW_SKYRIMSE;
		case NEXUS_ID::FALLOUT_4:
			return RAW_FALLOUT4;
		default:
			return nullptr;
	}
}

uint8_t BSA_Packer::raw_size(const NEXUS_ID nexus_id)
{
	switch (nexus_id) {
		case NEXUS_ID::OBLIVION:
			return RAW_TES4_SIZE;
		case NEXUS_ID::FALLOUT_3:
			return RAW_FO3_SIZE;
		case NEXUS_ID::NEW_VEGAS:
			return RAW_FNV_SIZE;
		case NEXUS_ID::SKYRIM:
			return RAW_TES5_SIZE;
		case NEXUS_ID::SKYRIM_SE:
			return RAW_SSE_SIZE;
		case NEXUS_ID::FALLOUT_4:
			return RAW_FO4_SIZE;
		default:
			return 0;
	}
}

BSA_Packer::PACKER_RESULT BSA_Packer::build_archive_all(QProgressDialog& progress,
														const QString& mod_path,
														BSArchiveAuto& archive) const
{
	PACKER_RESULT result = PACKER_RESULT::NULL_BSA;
	int incompressible_files = 0;
	int compressible_files = 0;
	// setup the progress bar for the iterations
	uint32_t count = progress.value();
	progress.setLabelText(tr("Marking valid files for packing."));
	// retrieve list of all filenames in the mod directory (no subdirectories)
	const QStringList& mod_directory_filenames = get_directory_files(mod_path);
	QDirIterator iterator(mod_path, QDirIterator::Subdirectories);
	while (iterator.hasNext()) {
		if (progress.wasCanceled())
			return PACKER_RESULT::NULL_BSA;

		const QString& filepath = iterator.next();
		const QFileInfo& fileinfo(filepath);
		const QString& filename = fileinfo.fileName();
		const bool blacklisted = mod_directory_filenames.contains(filename) || // ignore files within mod directory
			fileinfo.isDir() || // ignore directories
			filename == "." || filename == ".." || // ignore current dir and parent dir entries
			is_blacklisted_filetype(filename); // ignore user blacklisted file types
		
		progress.setValue(count++);
		if (blacklisted)
			continue;

		is_incompressible(filename) ?
			++incompressible_files :
			++compressible_files;
		archive.addFileFromDiskRoot(filepath);
	}
	progress.setLabelText(tr("Setting archive flags."));
	archive.setShareData(true);
	archive.setCompressed(!static_cast<bool>(incompressible_files));
	if (static_cast<bool>(incompressible_files) ||
		static_cast<bool>(compressible_files)) {
		result = PACKER_RESULT::SUCCESS;
		progress.setValue(0);
		progress.setMaximum(incompressible_files + compressible_files);
		progress.setLabelText(tr("Adding files to archive."));
	}
	return result;
}

BSA_Packer::PACKER_RESULT BSA_Packer::build_archive_textures(QProgressDialog& progress,
															 const QString* mod_path,
															 BSArchiveAuto& archive) const
{
	PACKER_RESULT result = PACKER_RESULT::NULL_BSA;
	int compressible_files = 0;
	// setup the progress bar for the iterations
	uint32_t count = progress.value();
	progress.setLabelText(tr("Marking valid texture files for packing."));
	// retrieve list of all filenames in the mod directory (no subdirectories)
	const QStringList& mod_directory_filenames = get_directory_files(*mod_path);
	QDirIterator iterator(*mod_path, QDirIterator::Subdirectories);
	while (iterator.hasNext()) {
		if (progress.wasCanceled())
			return PACKER_RESULT::NULL_BSA;

		const QString& filepath = iterator.next();
		const QFileInfo& fileinfo(filepath);
		const QString& filename = fileinfo.fileName();
		const bool blacklisted = mod_directory_filenames.contains(filename) || // ignore files within mod directory
			fileinfo.isDir() || // ignore directories
			filename == "." || filename == ".." || // ignore current dir and parent dir entries
			is_blacklisted_filetype(filename) || // ignore user blacklisted file types
			!filename.endsWith(".dds", Qt::CaseInsensitive);
		
		progress.setValue(count++);
		if (blacklisted)
			continue;

		++compressible_files;
		archive.addFileFromDiskRoot(filepath);
	}
	progress.setLabelText(tr("Setting archive flags."));
	archive.setShareData(true);
	archive.setCompressed(true);
	archive.setDDSCallback(DDSCallback, (void*)mod_path);
	if (static_cast<bool>(compressible_files)) {
		result = PACKER_RESULT::SUCCESS;
		progress.setValue(0);
		progress.setMaximum(compressible_files);
		progress.setLabelText(tr("Adding files to archive."));
	}

	return result;
}

BSA_Packer::PACKER_RESULT BSA_Packer::build_archive_no_textures(QProgressDialog& progress,
																const QString& mod_path,
																BSArchiveAuto& archive) const
{
	PACKER_RESULT result = PACKER_RESULT::NULL_BSA;
	int incompressible_files = 0;
	int compressible_files = 0;
	// setup the progress bar for the iterations
	uint32_t count = progress.value();
	progress.setLabelText(tr("Marking valid texture files for packing."));
	// retrieve list of all filenames in the mod directory (no subdirectories)
	const QStringList& mod_directory_filenames = get_directory_files(mod_path);
	QDirIterator iterator(mod_path, QDirIterator::Subdirectories);

	while (iterator.hasNext()) {
		if (progress.wasCanceled())
			return PACKER_RESULT::NULL_BSA;

		const QString& filepath = iterator.next();
		const QFileInfo& fileinfo(filepath);
		const QString& filename = fileinfo.fileName();
		const bool blacklisted = mod_directory_filenames.contains(filename) || // ignore files within mod directory
			fileinfo.isDir() || // ignore directories
			filename == "." || filename == ".." || // ignore current dir and parent dir entries
			is_blacklisted_filetype(filename) || // ignore user blacklisted file types
			filename.endsWith(".dds", Qt::CaseInsensitive);
		
		progress.setValue(count++);
		if (blacklisted)
			continue;

		is_incompressible(filename) ?
			++incompressible_files :
			++compressible_files;
		archive.addFileFromDiskRoot(filepath);
	}
	progress.setLabelText(tr("Setting archive flags."));
	archive.setShareData(true);
	archive.setCompressed(!static_cast<bool>(incompressible_files));
	if (static_cast<bool>(incompressible_files) ||
		static_cast<bool>(compressible_files)) {
		result = PACKER_RESULT::SUCCESS;
		progress.setValue(0);
		progress.setMaximum(incompressible_files + compressible_files);
		progress.setLabelText(tr("Adding files to archive."));
	}

	return result;
}

BSA_Packer::PACKER_RESULT BSA_Packer::can_overwrite_archives(const QString& mod_path,
															 const QString& archive_name) const
{
	if (archive_name.isEmpty() || archive_name.isNull())
		return PACKER_RESULT::INVALID_NAME;

	const QString& archive_fullname = mod_path + '/' + archive_name;
	const QFileInfo& archive_fileinfo(archive_fullname);
	if (!archive_fileinfo.exists() || !archive_fileinfo.isFile())
		return PACKER_RESULT::SUCCESS;

	const QString overwrite_message = tr("File \"") + archive_fullname + tr("\" already exists. Overwrite?");
	return QMessageBox::question(parentWidget(), tr("BSA Packer"), overwrite_message, QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Cancel ?
		PACKER_RESULT::CANCELLED_OVERWRITE :
		PACKER_RESULT::SUCCESS;
}

BSA_Packer::PACKER_RESULT BSA_Packer::check_archive_size(const QString& archive_fullname) const
{
	const uint32_t limit = get_archive_size_limit(m_nexus_id);
	const QFileInfo fileinfo(archive_fullname);
	if (fileinfo.size() > limit) {
		QFile::remove(archive_fullname);
		const QString limit_message = tr("The archive \"") + archive_fullname +
			tr("\" has exceeded the size limit of ") + QString::number(limit) +
			tr(" bytes.");
		QMessageBox::information(parentWidget(), tr("BSA Packer"), limit_message);
		return PACKER_RESULT::ARCHIVE_OVER_LIMIT;
	}

	const QString created_message = tr("Created archive \"") + archive_fullname + "\".";
	QMessageBox::information(parentWidget(), tr("BSA Packer"), created_message);
	return PACKER_RESULT::SUCCESS;
}

BSA_Packer::PACKER_RESULT BSA_Packer::create_archive(const QString& mod_path,
													 const QString& archive_base_name) const
{
	QProgressDialog progress(tr("Counting files."),
							 tr("Abort"),
							 0,
							 0,
							 parentWidget(),
							 Qt::MSWindowsFixedSizeDialogHint);
	progress.setModal(Qt::WindowModality::WindowModal);
	progress.setMinimumDuration(0);
	progress.setAutoClose(false);
	progress.setAutoReset(true);
	progress.show();

	QDirIterator it(mod_path, QDirIterator::Subdirectories);
	int file_count = 0;
	QString end;
	while (it.hasNext()) {
		it.next();
		file_count++;
	}
	progress.setMaximum(file_count);

	PACKER_RESULT result;
	const bool is_fo4 = m_nexus_id == NEXUS_ID::FALLOUT_4;

	// function pointer to the archive builder appriopriate for the current game
	PACKER_RESULT(BSA_Packer:: * build_func)(QProgressDialog&, const QString&, BSArchiveAuto&) const;
	build_func = is_fo4 ?
		&BSA_Packer::build_archive_no_textures :
		&BSA_Packer::build_archive_all;

	BSArchiveAuto archive(mod_path);
	// add all the files to the BSArchiveAuto
	result = (this->*build_func)(progress, mod_path, archive);
	if (result != PACKER_RESULT::SUCCESS)
		return result;

	// check that the name is valid, and that the user has not cancelled
	result = can_overwrite_archives(mod_path, archive_base_name + *m_extension);
	if (!(result == PACKER_RESULT::SUCCESS ||
		(is_fo4 && result == PACKER_RESULT::CANCELLED_OVERWRITE)))
		return result;

	// Reimplement so packing is done on a .bsa.part file, then renamed at end.
	// This way a previous archive won't be destroyed if the operations are cancelled.
	const QString& archive_fullname = mod_path + '/' + archive_base_name + *m_extension;
	if (result == PACKER_RESULT::SUCCESS) {
		int create_result = archive.create(progress, archive_fullname, find_archive_type(m_nexus_id));
		if (create_result == 1)
			return PACKER_RESULT::ABORTED_PACKING;
		archive.save();
		result = check_archive_size(archive_fullname);
	}

	// texture archive string not always used
	const QString& texture_archive_fullname = mod_path + '/' + archive_base_name + TEXTURES_BA2;
	PACKER_RESULT texture_result;
	if (is_fo4) {
		BSArchiveAuto texture_archive(mod_path);
		texture_result = build_archive_textures(progress, &mod_path, texture_archive);
		if (texture_result != PACKER_RESULT::SUCCESS)
			return result;

		// check that the name is valid, and that the user has not cancelled
		texture_result = can_overwrite_archives(mod_path, archive_base_name + TEXTURES_BA2);
		if (texture_result == PACKER_RESULT::CANCELLED_OVERWRITE)
			return result;

		int create_result = texture_archive.create(progress, texture_archive_fullname, baFO4dds);
		if (create_result == 1)
			return PACKER_RESULT::ABORTED_PACKING;
		texture_archive.save();
		texture_result = check_archive_size(texture_archive_fullname);
		if (texture_result != PACKER_RESULT::SUCCESS)
			return texture_result;
	}
	return result;
}

void BSA_Packer::create_dummy_plugin(const QString& mod_path,
									 const QString& archive_name_base) const
{
	// checks if ESM, ESP, or ESL already exists
	const std::array<QString, 3> file_list = {
		mod_path + '/' + archive_name_base + ".esm",
		mod_path + '/' + archive_name_base + ".esp",
		mod_path + '/' + archive_name_base + ".esl"
	};
	const size_t file_list_len = file_list.size();
	const int len = m_nexus_id == NEXUS_ID::SKYRIM_SE || m_nexus_id == NEXUS_ID::FALLOUT_4 ?
		file_list_len :
		file_list_len - 1;

	for (int i = 0; i < len; ++i) {
		const QFileInfo& file_info(file_list[i]);
		if (file_info.exists() && file_info.isFile())
			return;
	}

	// will create an ESP file
	const char* path = file_list[1].toStdString().c_str();
	FILE* file = nullptr;
	fopen_s(&file, path, "w");
	if (file != nullptr) {
		fwrite(raw_data(m_nexus_id),
			   sizeof(unsigned char),
			   raw_size(m_nexus_id),
			   file);
		fclose(file);
	}
}

bsa_archive_type_e BSA_Packer::find_archive_type(const NEXUS_ID nexus_id) const
{
	switch (nexus_id) {
		case NEXUS_ID::MORROWIND:
			return baTES3;
		case NEXUS_ID::OBLIVION:
			return baTES4;
		case NEXUS_ID::FALLOUT_3:
		case NEXUS_ID::NEW_VEGAS:
		case NEXUS_ID::SKYRIM:
			return baFO3;
		case NEXUS_ID::SKYRIM_SE:
			return baSSE;
		case NEXUS_ID::FALLOUT_4:
			return baFO4;
		case NEXUS_ID::UNDEFINED:
		default:
			return baNone;
	}
}

uint32_t BSA_Packer::get_archive_size_limit(const NEXUS_ID nexus_id) const
{
	return nexus_id != NEXUS_ID::FALLOUT_4 ? BSA_MAX_SIZE : BA2_MAX_SIZE;
}

QStringList BSA_Packer::get_directory_files(const QDir& mod_directory) const
{
	return QtConcurrent::blockingMapped(mod_directory.entryInfoList(QDir::Files),
										&BSA_Packer::get_filename_from_fileinfo);
}

QStringList BSA_Packer::get_mod_plugin_names(const QDir& mod_directory) const
{
	QStringList filenames = mod_directory.entryList(PLUGIN_TYPES, QDir::Files);
	for (QString& filename : filenames)
		filename = filename.replace(filename.lastIndexOf('.'), 4, *m_extension);
	filenames.removeDuplicates();
	return filenames << *m_new_archive;
}

QStringList BSA_Packer::get_valid_mods() const
{
	const IModList* const list = m_Organizer->modList();
	return QtConcurrent::blockingFiltered(list->allMods(), [&](const QString& mod) { return is_mod_state_valid(list, mod); });
}

void BSA_Packer::hide_loose_files(const QDir& mod_directory) const
{
	const QString& dir_name = mod_directory.absolutePath();
	QtConcurrent::blockingMap(mod_directory.entryList(QDir::Dirs | QDir::NoDotAndDotDot),
							  [&](const QString& sub_dir) { return hide_folder(dir_name, sub_dir); });
}

bool BSA_Packer::is_blacklisted_filetype(const QString& filename) const
{
	if (filename == nullptr)
		return true;

	const QStringList& blacklist = m_Organizer->pluginSetting(this->name(), SETTING_BLACKLISTED_FILES)
		.toString().split(';');
	for (const QString& ext : blacklist)
		if (filename.endsWith(ext, Qt::CaseInsensitive))
			return true;
	return false;
}

bool BSA_Packer::is_incompressible(const QString& filename) const
{
	for (const QString& str : INCOMPRESSIBLE_TYPES)
		if (filename.endsWith(str, Qt::CaseInsensitive))
			return true;
	return false;
}

void BSA_Packer::set_nexus_id(const NEXUS_ID nexus_id)
{
	switch (nexus_id) {
		case NEXUS_ID::MORROWIND:
		case NEXUS_ID::OBLIVION:
		case NEXUS_ID::FALLOUT_3:
		case NEXUS_ID::NEW_VEGAS:
		case NEXUS_ID::SKYRIM:
		case NEXUS_ID::SKYRIM_SE:
			{
				m_nexus_id = nexus_id;
				m_extension = &EXTENSION_BSA;
				m_new_archive = &NEW_BSA;
				return;
			}
		case NEXUS_ID::FALLOUT_4:
			{
				m_nexus_id = nexus_id;
				m_extension = &EXTENSION_BA2;
				m_new_archive = &NEW_BA2;
				return;
			}
		case NEXUS_ID::UNDEFINED:
		default:
			{
				return;
			}
	}
}

BSA_Packer::PACKER_RESULT BSA_Packer::valid_archive_name(const QString& mod_name,
														 QString* const archive_name_base) const
{
	// get the currently selected item from the listview
	const QListWidgetItem* const current_item = packer_dialog->read_selected_name();
	if (current_item == nullptr)
		return PACKER_RESULT::NO_ITEM_SELECTED;

	// get the chosen archive name from the currently selected listview item
	const QString& selected_archive_name = current_item->text();
	// check if it is the "new filename" text
	const bool needs_new_name = !static_cast<bool>(QString::compare(selected_archive_name, *m_new_archive));
	if (needs_new_name) {
		bool ok = false;
		const QString& name = QInputDialog::getText(parentWidget(),
													tr("BSA Packer"),
													tr("Archive name (no file extension):"),
													QLineEdit::Normal,
													mod_name,
													&ok).simplified();
		if (!ok || name.isEmpty() || name.isNull())
			return PACKER_RESULT::INVALID_NAME;

		*archive_name_base = name;
	} else {
		*archive_name_base = selected_archive_name.chopped(4); // trims the file extension off
	}
	return PACKER_RESULT::SUCCESS;
}

#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
Q_EXPORT_PLUGIN2(myTool, BSA_Packer)
#endif
